<!DOCTYPE html>
<html>
<head>
	<title>Curs 2019-2020</title>
</head>
<body>
<h1>PHP</h1>

<h3>Text per pantalla</h3>
<?php
    print("Hola món"); #Mostrem un conjunt de caràcters
    echo "<br>";
    print "Hola món";
    echo "<br>";
    echo "Hola món";
    echo "<br>";
    echo "Hola" . "Món"; #concatenem Strings
?>
<h3>Variables</h3>
<?php
    echo "Creem varibales <br>";
    $primera_variable = "Sóc una String!"; #Creem una cadena de caràcters
    $segona_variable = 0; #Creem un enter
    $salt_linia = "<br>"; #Creem una variable pels salts de línia
    echo "Mostrem la primera variable: " . $primera_variable . $salt_linia;
    echo "Mostrem la segona variable: " . $segona_variable . $salt_linia;
?>
<h3>Arrays</h3>
<?php
    echo "Creem una array" . $salt_linia;
    $primera_array = array("Pobles", "del", "món", "uniu-vos");#Posem els valors que vulguem
    echo "Imprimim valors array" . $salt_linia;
    echo $primera_array[0] . $primera_array[1] . $primera_array[2] . $primera_array[3] . $salt_linia;
?>
<h3>Bucles</h3>
<?php
    echo "Bucle for" . $salt_linia;
    for ($i = 0; $i < sizeof($primera_array); $i++){
        echo $primera_array[$i] . " ";
    }
    echo $salt_linia;
    echo "Bucle while" . $salt_linia;
    $i = 0;
    while ($i < sizeof($primera_array)){
        echo $primera_array[$i] . " ";
        $i++;
    }
    echo $salt_linia;
    echo "Bucle do-while" . $salt_linia;
    $i = 0;
    do{
        echo $primera_array[$i] . " ";
        $i++;
    }while($i < sizeof($primera_array)-1);
    echo $salt_linia;
?>
<h3>Funcions</h3>
<?php
    echo "Creem una funció" . $salt_linia;
    function mostrarValor($a){
        echo "El valor és:" . $a . "<br>";
    }
    function augmentarValor($a){
        return $a++;
    }
    $valor = augmentarValor(18);
    mostrarValor($valor);
?>
<h3>Condicionals</h3>
<?php
    $condicio = 5;
    if ($condicio == 5){
        echo "Funciona";
    }
?>
<?php
    $condicio++;
    if($condicio == 5):
?>
<h2>La condició és compleix</h2>
<?php
    else:
?>
<h2>La condició no és compleix</h2>
<?php
    endif;
?>

<h3>Botons</h3>

<?php
    echo $_SERVER["DOCUMENT_ROOT"];
?>
<?php
    include("./activitats/botons.php"); #Agafa tot el codi del fitxer botons
    echo $botons[4];
?>


</body>
</html>